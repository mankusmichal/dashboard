# Dashboard app

Spring boot and Vaadin app.

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `DemoApplication` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```

## Using the app

Go to `http://localhost:8080/`


## TODO
* Nazwa projektu nic nie mówi, nie ma README.md który by ułatwieł wejście w projekt komukolwiek
* IDE specific diractories should be removed from GIT
* Przy rejestracji nie mozna wybrac języka. Chciałbym np. angielski. Teraz Spring sam dobiera język na podstawie nagłowka HTTP.
Raczej tak to nie powiino działać w normalnych aplikacjach. Raczej zawsze daje się wybór użytkownikowi albo osobne domeny dla osobnej wersji.
 Teraz apliakcja mi się wywala bo:
```
SEVERE:
org.springframework.context.NoSuchMessageException: No message found under code 'newpost' for locale 'pl_PL'.
	at org.springframework.context.support.AbstractMessageSource.getMessage(AbstractMessageSource.java:161)
	at org.springframework.context.support.AbstractApplicationContext.getMessage(AbstractApplicationContext.java:1271)
	at com.example.demo.vaadin.DashboardUI.initConfigUIBtns(DashboardUI.java:151)
	at com.example.demo.vaadin.DashboardUI.init(DashboardUI.java:99)
	at com.vaadin.ui.UI.doInit(UI.java:771)
```
* Aplikacja raczej w takim momencie nie powinna sie wywalac tylko jakis defaultowy jezyk pokazac.
* Passwords can't be stored in plain text under any circumstances. Not even for dev purposes.
* Nie widze abys uzywal `ctrl + alt + o` - optymalizacja pakietow. Jest duzo zaimportowanych nieporzebnie.
* `// Base class without identity, we annotate it as @MappedSuperClass instead of @Entity` - this should be javadoc
* `PostFixtures` - you can make tests for that. There should not be commented out code.
* Do czego jest klasa `UserFixtures`? Chyba też chciałeś z tego zrobić test?
* W tym samym pakiecie domian masz serwisy in encje? Generalnie jak wygodniej. Ale u nas raczej jest dzielenie ze względu na funkcjonalności potem w razie potrzeby na warstwy aplikacji.
Idea która np. u nas senior developer Przemek promuje , fajnie opisana jest tutaj: https://www.youtube.com/watch?v=ILBX9fa9aJo
* Jak się dostać do bazy? Możesz łatow w Spring boot dodac webowe UI do H2
* Na przykladach vadina sa troche lepiej wygladajace strony. Raczej powinienes spróbować to odgapiac. To UI które zrobiłeś to jakaś nowa szkoła tworzenia UI.
* Wydaje mi sie ze baza dziala w trybie in memory, dane sie czyszca. Nie ma zadnych przykladowych ladowanych na starcie. Takie cos np. chociazby w jak robisz testy z odpytywaniem bazy przydaje.
* Fajnie aby byla mozliwosc przetestoawnia tego z normalna naza - np. PostgreSql zamiast H2. H2 jest bradziej do wykorzystania w testach, aplikacjach desktopowych.
* Posty ładujesz jako Eager - generalnie w jakiejs aplikacji prawidzwej trzeba by zrobic obsluge paginacji, aby z bazy zawsze wszytkiego nie tykał.
* Logowanie moze byc tez zrobione z uzyciem Google albo FB - czyli Spring Social. Może jakiś Kerberos ? Jak byśmy tworzyli takie aplikacje na użytek własnych to chyba miało by sens.
* Nie ma duzo ciekawych ofert pracy odnosci Vaadina tak mi się wydaje. Nadaje sie do jakichś prostych backedow, ale w większch systemach raczej potrzebujesz front end develoepra - React, Vue, Angular.
Chciałbyś się czegoś z tego nauczyć?
