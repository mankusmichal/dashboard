package com.example.demo.fixtures;

import com.example.demo.domain.User;
import com.example.demo.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

@Configuration
public class UserFixtures {
    @Autowired
    @Transactional // org.springframework.transaction.annotation.Transactional
    public void createUser(UserRepository userRepository) {
        User user = new User();
        user.setUsername("qwerty");
        user.setPassword("qwerty");
        userRepository.save(user);
    }

}
