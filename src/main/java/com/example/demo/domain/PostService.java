package com.example.demo.domain;


import com.example.demo.repo.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// This marks this class as a Spring Business Service
// Class path detection will create an instance of the class (singleton) and register it
@Service
public class PostService {

    // @Autowired Injects postRepository when instance of PostService is being created
    // Framework sees the embedded H2 db in the classpath and auto-connects. No additional config needed!
    private PostRepository postRepository;

    @Autowired
    public PostService(PostRepository postRepository) {
        this.postRepository = postRepository;
    }


    public List<Post> findAllPosts() {
        return postRepository.findAll();
    }

    public void addPost(Post post) {
        postRepository.save(post);
    }

    public Post findPostById(int id) {
        return postRepository.findById(id).orElse(null);
    }

    public void deletePostById(int id) {
        postRepository.deleteById(id);
    }
}
