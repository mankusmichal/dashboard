package com.example.demo.domain;


// Base class without identity, we annotate it as @MappedSuperClass instead of @Entity

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.ZonedDateTime;

@EntityListeners(AuditingEntityListener.class)
@MappedSuperclass
public abstract class AbstractEntity {
    @Id
    //@GeneratedValue autogenerates ids
    //@GeneratedValue(strategy= GenerationType.AUTO)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @CreatedDate
    private ZonedDateTime createdDate;
    @CreatedBy
    private User createdBy;
    @LastModifiedDate
    private ZonedDateTime modifiedDate;

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public ZonedDateTime getModifiedDate() {
        return modifiedDate;
    }

    public User getModifiedBy() {
        return modifiedBy;
    }

    @LastModifiedBy

    private User modifiedBy;

    public int getId() {
        return id;
    }

}
