package com.example.demo.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "Post")
public class Post extends AbstractEntity {

    private String header;
    @Lob
    private String content;
    @ManyToOne
    private User user;
    //@OneToOne(cascade = CascadeType.ALL)
    //private Comment comment;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @OrderColumn
    private List<Comment> comments;

    // private ZonedDateTime zoneDateTime = ZonedDateTime.now();

    public Post() {
    }

    public Post(String header, String content) {
        this.header = header;
        this.content = content;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Post getPost() {
        return this;
    }

/*    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }*/

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

//public ZonedDateTime getZoneDateTime() { return zoneDateTime; }

    //public void setZoneDateTime(ZonedDateTime zoneDateTime) { this.zoneDateTime = zoneDateTime; }

}
