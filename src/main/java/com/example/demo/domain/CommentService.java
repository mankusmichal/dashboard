package com.example.demo.domain;


import com.example.demo.repo.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {

    private CommentRepository commentRepository;

    @Autowired
    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }


    public List<Comment> findAllComments() {
        return commentRepository.findAll();
    }

    public void addComment(Comment comment) { commentRepository.save(comment); }

    public Comment findCommentById(int id) {
        return commentRepository.findById(id).orElse(null);
    }

    public void deleteCommentById(int id) {
        commentRepository.deleteById(id);
    }

}
