package com.example.demo.domain;


import javax.persistence.*;

@Entity
@Table(name="`Comment`")
public class Comment extends AbstractEntity {

    private String header;
    @Lob
    private String content;
    @ManyToOne
    private User user;
    @ManyToOne(cascade = CascadeType.ALL)
    Post post;

    public Comment(){}
    public Comment(String header, String content){
        this.header = header;
        this.content = content;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }
}
