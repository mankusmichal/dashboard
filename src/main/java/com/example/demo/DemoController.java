package com.example.demo;

import com.example.demo.domain.*;
import com.example.demo.repo.DtoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

// Spring MVC lets build server side code which maps to URL and provides responses

// Spring MVC annotation, marks the class as a Representational State Transfer controller
// Controller is a simple java class which maps a URI (Uniform Resource Identifier) and an http method to some functionality
// Allows to have a method within class which is linked to a request (HTTP requests & HTTP responses)
// Class path scan finds DemoController class and its methods and registers them
//@RestController
@Controller
public class DemoController {

    // Ask spring for a PostService singleton created once auto-detected
    // @Autowired injects the dependency
    @Autowired
    private PostService postService;
    @Autowired
    private UserService userService;
    @Autowired
    private final DtoRepository dtoRepository;


    public DemoController(DtoRepository dtoRepository) {
        this.dtoRepository = dtoRepository;
    }

    @RequestMapping("test0")
    public String returnString0() {
        return "Hi";
    }

    @GetMapping("test")
    public String returnString() {
        return "Hello world";
    }

    @GetMapping("test2")
    public Object returnJson() {
        return new Object() {
            public int number = 5;
            public String text = "text";
            public int[] integerArray = {1, 2, 3};
        };
    }

    @PostMapping(value = "test3", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String consumeJson(@RequestBody @Valid DTO dto) {
        dtoRepository.save(dto);
        return dto.getNumber() + dto.getText();
    }

    @GetMapping("test5")
    public List<DTO> returnDtoList() {
        return dtoRepository.findAll();
    }

    // List<PostDisplay> type return object will get converted to JSON automatically and sent back as a HTTP response
    // Spring MVC converted into JSON automatically
    // Creates a new list and sends it back
    @RequestMapping("/getposts")
    public List<Post> getAllPosts() {
        return postService.findAllPosts();
    }

    @GetMapping("/getpostbyid/{id}")
    public Post getPostById(@PathVariable int id) {
        return postService.findPostById(id);
    }

    @RequestMapping("/addpostmanually/{title}/{post}")
    public String addPostWithoutPostman(@PathVariable String title, @PathVariable String post) {
        Post newPost = new Post(title, post);
        postService.addPost(newPost);
        return "PostDisplay '" + newPost.getContent() + "' added successfully!";
    }

    @RequestMapping("/deletepostbyid/{id}")
    public String deletePostById(@PathVariable int id) {
        String title = postService.findPostById(id).getHeader();
        postService.deletePostById(id);
        return "PostDisplay titled: '" + title + "' deleted sucessfully!";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/addpost")
    public void addPost(@RequestBody Post post) {
        postService.addPost(post);
    }

    @GetMapping("/register")
    public String registerForm(Model model){
        model.addAttribute("user", new User());
        return "views/registerForm";
    }


    // https://github.com/sambaf/NHSystem/tree/master/src/main/java/org/sambasoft
    @PostMapping("/register")
    public String registerUser(@Valid User user, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors()){
            return "views/registerForm";
        }
        if(userService.loadUserByUsername(user.getUsername()) != null){
            model.addAttribute("exist", true);
            return "views/registerForm";
        }
        userService.createUser(user);
        return "views/success";

    }


    @RequestMapping("/test-locale")
    @ResponseBody
    public String testLocale (Locale locale) {
        return String.format("Request received. Language: %s, Country: %s %n",
                locale.getLanguage(), locale.getDisplayCountry());
    }

}





