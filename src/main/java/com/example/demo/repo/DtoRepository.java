package com.example.demo.repo;


import com.example.demo.domain.DTO;
import org.springframework.data.jpa.repository.JpaRepository;


// JPA Java Persistence API allows to do ORM (Object Relational Mapping) when u r connection to a database
// Most databases are SQL (relational database - tables and keys)
// When you need to connect you have to be able to use JDBC (run SQL queries on the relational db)
// Once you get the results from queries you convert them into object instances
// ORM allows us to map entity classes into SQL tables (you have to provide metadata on your entity classes)
// JPA is a way to use ORM, JPA is a API/specification that allows you to configure your entity classes and give it to a framework
// Spring Data JPA is a project which lets you make working with ORM tools even easier
public interface DtoRepository extends JpaRepository<DTO, Integer> {}