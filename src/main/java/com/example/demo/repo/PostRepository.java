package com.example.demo.repo;

import com.example.demo.domain.Post;
import org.springframework.data.jpa.repository.JpaRepository;

// JpaRepository<EntityClass, IdType> contains logic for any entity class = CRUD methods
public interface PostRepository extends JpaRepository<Post, Integer> {
}
