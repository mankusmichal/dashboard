package com.example.demo.vaadin;


import com.example.demo.domain.Post;
import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.Binder;
import com.vaadin.ui.*;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;


import java.util.function.Consumer;

public class PostForm extends FormLayout {

    private MessageSource messageSource;

    private Post post;

    private Label headerLabel;
    private TextField tf;
    private Label rtaLabel;
    private RichTextArea rta;
    private Button submitButton;
    private Consumer<Post> postConsumer;
    private Binder<Post> bdr;

    public PostForm(Post post, Consumer<Post> postConsumer, MessageSource messageSource) {
        this.post = post;
        this.postConsumer = postConsumer;
        this.messageSource = messageSource;
        this.setWidth("1050");
        configurePostForm();
    }

    private void configurePostForm() {
        // header
        initAndConfigHeader();
        // content
        initAndConfigContentRTA();
        // load old header & content if exists
        loadOldInputIfExists();
        // bind richTextArea input fields to post fields
        bindInputfieldsToPost();
        // initialize and config submit button
        initAndConfigSubmitButton();
        // add above configured components to the form layout
        addComponents(headerLabel, tf, rtaLabel, rta, submitButton);
    }

    private void initAndConfigHeader() {
        headerLabel = new Label(messageSource.getMessage("header", null, UI.getCurrent().getLocale()));
        tf = new TextField();
        tf.setWidth("1000");
    }

    private void initAndConfigContentRTA(){
        rtaLabel = new Label(messageSource.getMessage("content", null, UI.getCurrent().getLocale()));
        rta = new RichTextArea();
        rta.setWidth("1000");
        rta.setHeight("600");
    }

    private void initAndConfigSubmitButton() {
        submitButton = new Button(messageSource.getMessage("submit", null, UI.getCurrent().getLocale()), click -> {

            bdr.writeBeanIfValid(post);
            postConsumer.accept(post);
        });
    }

    private void loadOldInputIfExists() {
        if (post.getHeader() != null) {
            tf.setValue(post.getHeader());
        }
        if (post.getContent() != null) {
            rta.setValue(post.getContent());
        }
    }

    private void bindInputfieldsToPost() {
        bdr = new BeanValidationBinder<>(Post.class);
        bdr.bind(rta, "content");
        bdr.bind(tf, "header");
    }

}
