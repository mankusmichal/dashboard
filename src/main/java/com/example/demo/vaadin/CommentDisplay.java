package com.example.demo.vaadin;


import com.example.demo.domain.Comment;
import com.example.demo.domain.CommentService;
import com.example.demo.domain.Post;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;

import java.time.format.DateTimeFormatter;
import java.util.function.Consumer;

class CommentDisplay extends VerticalLayout {

    private MessageSource messageSource;

    //postToDisplay
    private Post post;

    //postDisplayLayoutComponents
    private Label label;
    private TextArea header;
    private TextArea content;

    //layoutContainingButtons
    private HorizontalLayout postButtonsLayout;
    private Button edit;
    private Button delete;
    private Consumer<Post> postConsumerUpdate;
    private Consumer<Post> postConsumerCommentEdit;

    CommentDisplay(Post post, Consumer<Post> postConsumerUpdate, Consumer<Post> postConsumerCommentEdit) {
        this.post = post;

        this.postConsumerUpdate = postConsumerUpdate;
        this.postConsumerCommentEdit = postConsumerCommentEdit;

        configurePostDisplay();
    }

    private void configurePostDisplay() {

        VerticalLayout commentsLayout = new VerticalLayout();
        //initAndConfigPostButtonsLayout();

        for (int i = 0; i<post.getComments().size(); i++){
            initAndConfigLabel(post.getComments().get(i));
            initAndConfigContent(post.getComments().get(i));

            commentsLayout.addComponent(label);
            commentsLayout.addComponent(content);
            initAndConfigPostButtonsLayout(post.getComments().get(i));
            if (i == post.getComments().size()-1){
            commentsLayout.addComponent(postButtonsLayout);
            commentsLayout.setComponentAlignment(postButtonsLayout, Alignment.MIDDLE_RIGHT);}
        }

        // example: USER: a | DATE: FRIDAY | POST ID: 2
        //initAndConfigLabel(comment);
        // post header
        //initAndConfigHeader(comment);
        // post content
        //initAndConfigContent(comment);
        // post buttons (comment, edit, del)
        //initAndConfigPostButtonsLayout();
        // adding above initialized components
        //commentsLayout.addComponent(label);
        //addComponent(header);
        //commentsLayout.addComponent(content);
        //commentsLayout.addComponent(postButtonsLayout);
        //commentsLayout.setComponentAlignment(postButtonsLayout, Alignment.MIDDLE_RIGHT);
        // if relation in the DB exists and reflects in post object commentDisplay is added as last component
        // styling
        commentsLayout.setSpacing(false);
        commentsLayout.setMargin(false);

        addComponent(commentsLayout);
        setSpacing(false);
        setMargin(false);

    }


    private void initAndConfigPostButtonsLayout(Comment comment) {
        postButtonsLayout = new HorizontalLayout();
        buttonsInitAndConfig(comment);
        postButtonsLayoutConfig();
        postButtonsLayout.setSpacing(false);
    }

    private void initAndConfigContent(Comment comment) {
        content = new TextArea();
        content.setValue(comment.getContent());
        content.setWidth("100%");
        content.setHeight("100");
        content.setStyleName(ValoTheme.TEXTAREA_SMALL);
        content.setReadOnly(true);
    }

/*    private void initAndConfigHeader(Comment comment) {
        header = new TextArea();
        header.setValue(comment.getHeader());
        header.setWidth("100%");
        header.setHeight("40");
        header.setStyleName(ValoTheme.TEXTAREA_BORDERLESS);
        header.setReadOnly(true);
    }*/

    private void initAndConfigLabel(Comment comment) {
        label = new Label();
        label.setContentMode(ContentMode.HTML);
        label.setValue("<b>USER:</b> " + comment.getUser().getUsername() +
                " | <b>DATE:</b> " + comment.getCreatedDate().format(DateTimeFormatter.ISO_TIME).substring(0, 8) + " (" + comment.getCreatedDate().format(DateTimeFormatter.ISO_LOCAL_DATE) + ") " +
                " | <b>ID:</b> " + comment.getId());
    }

    private void postButtonsLayoutConfig() {
        postButtonsLayout.addComponents(edit);
        postButtonsLayout.addComponents(delete);
    }

    private void buttonsInitAndConfig(Comment comment) {
        //editButton
        edit = new Button("EDIT", click -> postConsumerCommentEdit.accept(post));
        edit.setStyleName(ValoTheme.BUTTON_FRIENDLY);
        edit.setWidth("100");
        //deleteButton
        delete = new Button("DEL", click -> {
            post.getComments().remove(comment);
            postConsumerUpdate.accept(post);
        });
        delete.setStyleName(ValoTheme.BUTTON_DANGER);
        delete.setWidth("100");

    }

}
