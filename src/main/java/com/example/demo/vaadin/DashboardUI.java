package com.example.demo.vaadin;

import com.example.demo.domain.*;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.*;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.core.ResolvableType;
import org.springframework.lang.Nullable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.function.Consumer;

@Push
@SpringUI
@Theme("tests-valo-facebook")
public class DashboardUI extends UI implements MessageSourceAware {

    static Set<UI> allUIs = new HashSet<>();

    private String label = "<font face='Verdana' size='6' color='#26a0f2'><b>B<font color='#006bb2'>[]</font>ARD<font size='2' color='#006bb2'>CRUD</font>";
    private Label nameLabel;

    private Button newPostBtn;
    private Button languageBtn;
    private Button logoutBtn;

    static User user;

    private VerticalLayout verticalLayout;
    private VerticalLayout innerVerticalLayout;
    private VerticalSplitPanel verticalSplitPanel;
    private HorizontalLayout horizontalLayout;
    private HorizontalLayout horizontalLayout1;
    private Window window;

    private Consumer<Post> postConsumer;
    private Consumer<Post> postConsumerDelete;
    private Consumer<Post> postConsumerEdit;
    private Consumer<Post> postConsumerComment;
    private Consumer<Post> postConsumerUpdate;
    private Consumer<Post> postConsumerCommentEdit;

    static PostService postService;
    static MessageSource messageSource;


    {
        // translations should depend session and not on UI
        addAttachListener(event -> {
            synchronized (allUIs) {
                allUIs.forEach(ui -> {
                    new Thread(() -> ui.access(() -> Notification.show("New user logged in."))).start();
                });
                allUIs.add(this);
            }
        });

        addDetachListener(event -> {
            synchronized (allUIs) {
                allUIs.remove(this);
                allUIs.forEach(ui -> {
                    new Thread(() -> ui.access(() -> Notification.show("User logged out."))).start();
                });
            }
        });
    }

    @Autowired
    public DashboardUI(PostService postService) {
        this.postService = postService;
    }

    @Override
    protected void init(VaadinRequest request) {
        // get user from request
        UsernamePasswordAuthenticationToken principal = (UsernamePasswordAuthenticationToken) request.getUserPrincipal();
        user = (User) principal.getPrincipal();
        // configure post consumers and put them into an array
        initConfigAndGetPostConsumers();
        // UI Label (1st element on the top right)
        nameLabel = new Label(label, ContentMode.HTML);
        // inits and configs UI buttons (new post, change language, log out)
        initConfigUIBtns();
        // inits and configs vertical layout containing change language & log out buttons
        initConfigVerticalLayout();
        // init and config horizontal layout containing new post and vertical layout (change language & log out buttons) - top right
        horizontalLayout1 = new HorizontalLayout();
        horizontalLayout1.addComponents(newPostBtn, verticalLayout);
        horizontalLayout1.setSpacing(false);
        // inits and configs horizontal layout containing name label (logo) and main buttons - whole top
        initConfigHorizontalLayout();
        // inits and configs inner vertical layout (contains posts and their comments) - whole bottom
        initConfigInnerVerticalLayout();
        // adds existing posts to inner vertical layout
        loadPostsFromDBIntoVerticalLayout();
        // vertical split panel composition
        verticalSplitPanel = new VerticalSplitPanel();
        // 1st element (top)
        verticalSplitPanel.setFirstComponent(horizontalLayout);
        // 2nd element (bottom)
        verticalSplitPanel.setSecondComponent(innerVerticalLayout);
        verticalSplitPanel.setSplitPosition(8);
        verticalSplitPanel.setSizeFull();
        verticalSplitPanel.setLocked(true);
        // final setup
        setContent(verticalSplitPanel);
    }

    private void initConfigInnerVerticalLayout() {
        innerVerticalLayout = new VerticalLayout();
        innerVerticalLayout.setSpacing(false);
        innerVerticalLayout.setWidth("100%");
        innerVerticalLayout.setMargin(false);
    }

    private void initConfigHorizontalLayout() {
        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(nameLabel);
        horizontalLayout.addComponent(horizontalLayout1);
        horizontalLayout.setWidth("100%");
        horizontalLayout.setComponentAlignment(nameLabel, Alignment.MIDDLE_LEFT);
        horizontalLayout.setComponentAlignment(horizontalLayout1, Alignment.TOP_RIGHT);
        horizontalLayout.setHeight("60");
    }

    private void initConfigVerticalLayout() {
        verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(logoutBtn);
        verticalLayout.addComponent(languageBtn);
        verticalLayout.setMargin(false);
        verticalLayout.setSpacing(false);
    }

    private void initConfigUIBtns() {
        newPostBtn = new Button("<font size='5'><b>" + messageSource.getMessage("newpost", null, getLocale()) + "</b></font>", click -> {
            PostForm pf = new PostForm(new Post(), postConsumer, messageSource);
            createNewWindow();
            window.setContent(pf);
            getUI().addWindow(window);
        });
        newPostBtn.setWidth("400");
        newPostBtn.setHeight("60");
        newPostBtn.setCaptionAsHtml(true);
        newPostBtn.setIcon(VaadinIcons.ENTER);
        newPostBtn.addStyleName(ValoTheme.BUTTON_PRIMARY);

        languageBtn = new Button(messageSource.getMessage("langver", null, getLocale()), click -> {
            if(!getSession().getLocale().equals(Locale.ENGLISH) && !getSession().getLocale().equals(Locale.GERMAN)){
                getSession().setLocale(Locale.ENGLISH);
            }
            if (getSession().getLocale().equals(Locale.ENGLISH)) {
                getSession().setLocale(Locale.GERMAN);
            } else {
                getSession().setLocale(Locale.ENGLISH);
            }
            Notification.show("Language changed - " + getSession().getLocale().toString());
            Page.getCurrent().reload();
        });
        languageBtn.setWidth("400");
        languageBtn.addStyleName(ValoTheme.BUTTON_FRIENDLY);

        logoutBtn = new Button("(" + user.getUsername().toUpperCase() + ") LOG OUT", click -> {
            getPage().setLocation("/logout");
        });
        logoutBtn.addStyleName(ValoTheme.BUTTON_DANGER);
        logoutBtn.setWidth("400");
    }

    private void initConfigAndGetPostConsumers() {
// stworzyc klase dostarczajaca consumery (singleton) posiadajacy postService, messageSource

        postConsumer = post -> {
            if (post.getUser() == null) {
                post.setUser(user);
            }
            if (post.getComments() != null) {
                for (int i = 0; i < post.getComments().size(); i++) {
                    if (post.getComments().get(i).getUser() == null) {
                        post.getComments().get(i).setUser(user);
                    }
                }
            }
            postService.addPost(post);
            window.close();
            loadPostsFromDBIntoVerticalLayout();
        };

        postConsumerUpdate = post -> {
            postService.addPost(post);
            loadPostsFromDBIntoVerticalLayout();
        };

        postConsumerEdit = post -> {
            PostForm pf = new PostForm(post, postConsumer, messageSource);
            createNewWindow();
            window.setContent(pf);
            getUI().addWindow(window);
        };

        postConsumerComment = post -> {
            CommentForm cf = new CommentForm(post, postConsumer, messageSource, false);
            createNewWindow();
            window.setContent(cf);
            getUI().addWindow(window);
            // stworzyc fabryke postdisplay
            // stworz mi formularz
        };

        postConsumerCommentEdit = post -> {
            CommentForm cf = new CommentForm(post, postConsumer, messageSource, true);
            window = new Window();
            window.setContent(cf);
            getUI().addWindow(window);
        };

        postConsumerDelete = post -> {
            postService.deletePostById(post.getId());
            loadPostsFromDBIntoVerticalLayout();
        };


    }

    //create object factory with generic PostDisplay
    //obtain postDisplay bean in a loop and set all the fields passed previously through constructor
    //this solves PostService & MessageSource visibility issue
    //this does not solve current UI active user visibility issue in instances of PostDisplay //@Autowire Dashboard i pozyskaj usera przez request

    private void loadPostsFromDBIntoVerticalLayout() {
        innerVerticalLayout.removeAllComponents();
        List<Post> postList = postService.findAllPosts();
        for (int i = 0; i < postList.size(); i++) {
            PostDisplay postDisplay = new PostDisplay(postList.get(i), postConsumer, postConsumerUpdate, postConsumerEdit, postConsumerComment, postConsumerDelete, postConsumerCommentEdit);
            innerVerticalLayout.addComponent(postDisplay);
        }
    }

    // initializing create&edit post/comment floating window
    public void createNewWindow() {
        window = new Window();
        window.setModal(true);
    }

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }



}

