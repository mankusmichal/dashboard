package com.example.demo.vaadin;


import com.example.demo.domain.Comment;
import com.example.demo.domain.Post;
import com.vaadin.data.BeanValidationBinder;
import com.vaadin.data.Binder;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class CommentForm extends FormLayout {

    private MessageSource messageSource;

    private Post post;
    private Comment comment;

    private Label headerLabel;
    private TextField tf;
    private Label rtaLabel;
    private RichTextArea rta;
    private Button submitButton;
    private Consumer<Post> postConsumer;

    private Binder<Comment> bdr;

    public CommentForm(Post post, Consumer<Post> postConsumer, MessageSource messageSource, boolean editOperation) {
        this.post = post;
        this.postConsumer = postConsumer;
        this.messageSource = messageSource;
        this.setWidth("1050");

        configurePostForm();

        if(editOperation) loadLastComment();
    }

    private void configurePostForm() {
        // header
        initAndConfigHeader();
        // content
        initAndConfigContentRTA();
        // load old header & content if exists
        //loadOldInputIfExists();
        // bind richTextArea input fields to post fields
        bindInputfieldsToPost();
        // initialize and config submit button
        initAndConfigSubmitButton();
        // add above configured components to the form layout
        //addComponents(headerLabel, tf, rtaLabel, rta, submitButton);
        addComponents(rtaLabel, rta, submitButton);
    }

    private void initAndConfigHeader() {
        headerLabel = new Label(messageSource.getMessage("header", null, UI.getCurrent().getLocale()));
        //headerLabel = new Label("HEADER");

        tf = new TextField();
        tf.setWidth("1000");
    }

    private void initAndConfigContentRTA() {
        rtaLabel = new Label(messageSource.getMessage("content", null, UI.getCurrent().getLocale()));
        //rtaLabel = new Label("CONTENT");

        rta = new RichTextArea();
        rta.setWidth("1000");
        rta.setHeight("600");
    }

    private void initAndConfigSubmitButton() {

        submitButton = new Button(messageSource.getMessage("submit", null, UI.getCurrent().getLocale()), click -> {
            if (post.getComments().isEmpty()) {
                post.setComments(new ArrayList<>());
            }
            if (comment == null) {
                comment = new Comment();
            }
            List<Comment> comments = post.getComments();
            comments.remove(comment);
            bdr.writeBeanIfValid(comment);
            comments.add(comment);
            post.setComments(comments);
            postConsumer.accept(post);

        });
    }

    private void loadLastComment() {
        this.comment = post.getComments().get(post.getComments().size() - 1);
        rta.setValue(post.getComments().get(post.getComments().size() - 1).getContent());
    }

    private void bindInputfieldsToPost() {
        bdr = new BeanValidationBinder<>(Comment.class);
        bdr.bind(rta, "content");
        bdr.bind(tf, "header");
    }



}
