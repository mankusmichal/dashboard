package com.example.demo.vaadin;

import com.example.demo.domain.Post;
import com.example.demo.domain.PostService;
import com.example.demo.domain.User;
import com.example.demo.repo.PostRepository;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Component;

import javax.persistence.OneToMany;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.Consumer;

//@SpringComponent
//@UIScope
//@Scope("prototype")
public class PostDisplay extends VerticalLayout {

    //@Autowired
    private PostService postService;

    //@Autowired
    MessageSource messageSource;

    //postToDisplay
    private Post post;

    //postDisplayLayoutComponents
    private Label label;
    private TextArea header;
    private TextArea content;

    //layoutContainingButtons
    private HorizontalLayout postButtonsLayout;
    private Button comment;
    private Button edit;
    private Button delete;
    private Consumer<Post> postConsumerDelete;
    private Consumer<Post> postConsumerEdit;
    private Consumer<Post> postConsumerComment;
    private Consumer<Post> postConsumerUpdate;
    private Consumer<Post> postConsumerCommentEdit;

    public PostDisplay(Post post, Consumer<Post> postConsumer, Consumer<Post> postConsumerUpdate, Consumer<Post> postConsumerEdit, Consumer<Post> postConsumerComment, Consumer<Post> postConsumerDelete, Consumer<Post> postConsumerCommentEdit) {
        this.post = post;


        this.postConsumerUpdate = postConsumerUpdate;
        this.postConsumerEdit = postConsumerEdit;
        this.postConsumerComment = postConsumerComment;
        this.postConsumerDelete = postConsumerDelete;
        this.postConsumerCommentEdit = postConsumerCommentEdit;

        this.postService = DashboardUI.postService;
        this.messageSource = DashboardUI.messageSource;

        configurePostDisplay();

    }

    void configurePostDisplay() {
        // example: USER: a | DATE: FRIDAY | POST ID: 2
        initAndConfigLabel();
        // post header
        initAndConfigHeader();
        // post content
        initAndConfigContent();
        // post buttons (comment, edit, del)
        initAndConfigPostButtonsLayout();
        // adding above initialized components
        addComponent(label);
        addComponent(header);
        addComponent(content);
        addComponent(postButtonsLayout);
        setComponentAlignment(postButtonsLayout, Alignment.MIDDLE_RIGHT);
        // if relation in the DB exists and reflects in post object commentDisplay is added as last component
        initConfigAndSetCommentDisplay();
        // styling
        setSpacing(false);
        setMargin(false);
    }

    private void initAndConfigPostButtonsLayout() {
        postButtonsLayout = new HorizontalLayout();
        buttonsInitAndConfig();
        postButtonsLayoutConfig();
        postButtonsLayout.setSpacing(false);
    }

    private void initAndConfigContent() {
        content = new TextArea();
        content.setValue(post.getContent());
        content.setWidth("100%");
        content.setHeight("100");
        content.setStyleName(ValoTheme.TEXTAREA_SMALL);
        content.setReadOnly(true);
    }

    private void initAndConfigHeader() {
        header = new TextArea();
        header.setValue(post.getHeader());
        header.setWidth("100%");
        header.setHeight("40");
        header.setStyleName(ValoTheme.TEXTAREA_BORDERLESS);
        header.setReadOnly(true);
    }

    private void initAndConfigLabel() {
        label = new Label();
        label.setContentMode(ContentMode.HTML);
        label.setValue("<b>USER:</b> " + post.getUser().getUsername() + " | <b>DATE:</b> " + post.getCreatedDate().format(DateTimeFormatter.ISO_TIME).toString().substring(0, 8) + " (" + post.getCreatedDate().format(DateTimeFormatter.ISO_LOCAL_DATE) + ") " + " | <b>ID:</b> " + post.getId());
    }

    private void postButtonsLayoutConfig() {
        postButtonsLayout.addComponents(comment);
        if (post.getUser().getUsername().equalsIgnoreCase(DashboardUI.user.getUsername())) {
            //post.getComments().isEmpty() &&
            postButtonsLayout.addComponents(edit, delete);
        }
    }

    private void buttonsInitAndConfig() {
        //commentButton
        comment = new Button("CMT", click -> {
            this.post = postService.findPostById(post.getId());
            postConsumerComment.accept(post);
        });

/*        comment = new Button(messageSource.getMessage("cmt", null, getLocale()), click -> {
            postConsumerComment.accept(post);
        });*/

        comment.setWidth("100");
        comment.setStyleName(ValoTheme.BUTTON_PRIMARY);
        //editButton
        edit = new Button("EDIT", click -> postConsumerEdit.accept(post));

        edit.setStyleName(ValoTheme.BUTTON_FRIENDLY);
        edit.setWidth("100");
        //deleteButton
        delete = new Button("DEL", click -> {
            postConsumerDelete.accept(this.post);
        });
        delete.setStyleName(ValoTheme.BUTTON_DANGER);
        delete.setIcon(VaadinIcons.TRASH);
        delete.setWidth("100");
    }



    private void initConfigAndSetCommentDisplay() {
        if (!post.getComments().isEmpty()) {
            CommentDisplay commentDisplay = new CommentDisplay(post, postConsumerUpdate, postConsumerCommentEdit);
            commentDisplay.setWidth("95%");
            addComponent(commentDisplay);
            setComponentAlignment(commentDisplay, Alignment.TOP_RIGHT);
        }
    }



}


// listenery
// pozbyc consumerow
// poczytac o decoupling
// ui nasluchuje na event zmiany stanu
// postdisplayfactory
